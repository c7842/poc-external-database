# Consul

## Generate secrets for Consul

Get the Consul certficates and its Gossip Key, copy them into `./secrets/certs`.

Generate the Kubernetes secrets:

```sh
kubectl -n consul create secret generic consul-ca-cert --dry-run=client -o=yaml --from-file=tls.crt=./secrets/certs/consul-agent-ca.pem > secrets/consul-ca-cert.yaml
kubectl -n consul create secret generic consul-ca-key --dry-run=client -o=yaml --from-file=tls.key=./secrets/certs/consul-agent-ca-key.pem > secrets/consul-ca-key.yaml
kubectl -n consul create secret generic gossip-encryption-key --dry-run=client -o=yaml --from-file=key=secrets/certs/gossip.key > secrets/gossip-encryption-key.yaml
```

## Consul as a service mesh provider

Prepare your K8s cluster:

```sh
kubectl create namespace consul
kubectl apply -f gossip-encryption-key.yaml
kubectl apply -f consul-ca-cert.yaml
kubectl apply -f consul-ca-key.yaml
kubectl label namespace default connect-inject=enabled
```

Install Consul support via `helm`:

```sh
helm repo add hashicorp https://helm.releases.hashicorp.com
helm search repo hashicorp/consul
helm install consul hashicorp/consul --namespace consul --values values.yaml
```

Use the following for upgrading:

```sh
helm upgrade consul hashicorp/consul --namespace consul --values values.yaml
```

Check the results in your browser : [http://192.168.56.51:8500/ui/local/services](http://192.168.56.51:8500/ui/local/services). Notes, this URL may vary depending on the IP address of the Consul node holding the UI.

Add an `intention` denying all trafic to everywhere:

```sh
kubectl apply -f k8s-consul_addons/deny-all.yaml
```

Run the example ( borrowed from [hashicorp-demoapp/hashicups-setups](https://github.com/hashicorp-demoapp/hashicups-setups/tree/v1.0.0/local-k8s-consul-deployment/k8s) ):

```sh
kubectl apply -f hashicups
```

You should play with `intentions` to allow traffic between pods. Tips: it is possible to do it directly from the graphs, try it it's magical !

For example, open the frontend service page [http://192.168.56.51:8500/ui/local/services/frontend/topology](http://192.168.56.51:8500/ui/local/services/frontend/topology) then click on the red arrow between `frontend` and `public-api` to create the appropriate intention.

Once done with the intentions, forward the port to test the applicaton:

```sh
kubectl port-forward service/frontend 8080:80 > /dev/null 2>&1 &
```

It should be available at [http://localhost:8080](http://localhost:8080).

## Move on to the next scenario

The idea is to use a database outside of the cluster. Start it locally:

```sh
podman volume create consul-pgdata
# Creates a volume for the data
podman run -d --rm -p "5432:5432" \
    -e "POSTGRES_DB=products" \
    -e "POSTGRES_USER=postgres" \
    -e "POSTGRES_PASSWORD=password" \
    -v "consul-pgdata:/var/lib/postgresql/data" \
    hashicorpdemoapp/product-api-db:v0.0.16
```

Note, it rely on the same setup used previously. The database will accessible locally on the `5432` port.

Let's proceed with the external database registration in Consul.

Create a JSON content `k8s-consul_addons/register-external-postgres.json`:

```json
{
  "Node": "localhost",
  "Address": "192.168.56.1",
  "NodeMeta": {
    "external-node": "true",
    "external-probe": "true"
  },
  "Service": {
    "ID": "external-postgres1",
    "Service": "external-postgres",
    "Port": 5432
  }
```

Create a JSON content `k8s-consul_addons/deregister-external-postgres.json` :

```json
{
  "Node": "localhost",
  "Address": "192.168.56.1",
  "ServiceID": "external-postgres1"
}
```

Note you may need to adapt the fields `Address` depending on your network configuration.

Register the node in Consul:

```sh
curl --request PUT --data @k8s-consul_addons/register-external-postgres.json http://192.168.56.51:8500/v1/catalog/register
```

For the record, you can deregister the external database with the following:

```sh
curl --request PUT --data @k8s-consul_addons/deregister-external-postgres.json http://192.168.56.51:8500/v1/catalog/deregister
```

Add the `TerminatingGateway` in the K8s cluster

Create a YAML content `k8s-consul_addons/external-postgres-terminating-gateway.yaml`:

```yaml
---
apiVersion: consul.hashicorp.com/v1alpha1
kind: TerminatingGateway
metadata:
  name: terminating-gateway
spec:
  services:
  - name: external-postgres
```

Apply the `TerminatingGateway`:

```sh
kubectl apply -f k8s-consul_addons/external-postgres-terminating-gateway.yaml
```

Let's be dirty and delete the internal database:

```sh
kubectl delete -f hashicups/postgres.yaml
```

It should bring chaos to application, let's solve that, switch the upstream `product-api` from `postgres` to `external-postgres`:

```sh
kubectl apply -f k8s-consul_addons/updated-product-api.yaml
```

Here the pod of the `product-api` is annotated to make use of the external database. The change may take some time, as it wait for the pod to restart.

Once the update is visible in the Consul UI, allow the traffic by adding the proper intention.

Et voilà ! The `frontend` should be back online using an external database.
